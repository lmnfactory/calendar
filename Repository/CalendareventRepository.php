<?php

namespace Lmn\Calendar\Repository;

use Lmn\Core\Lib\Repository\AbstractEventEloquentRepository;
use Lmn\Core\Lib\Repository\Criteria\CriteriaService;
use Lmn\Calendar\Database\Model\Calendarevent;
use Lmn\Core\Lib\Facade\Generator;

class CalendareventRepository extends AbstractEventEloquentRepository {

    public function __construct(CriteriaService $criteriaService) {
        parent::__construct($criteriaService);
    }

    public function getModel() {
        return Calendarevent::class;
    }

    public function create($data) {
        $model = $this->getModel();
        $instance = new $model($data);
        $instance->public_id = Generator::uniqueString('calendarevent', 'public_id', 24);
        $instance->save();
        $this->onCreate($instance, $data);
        return $instance;
    }
}
