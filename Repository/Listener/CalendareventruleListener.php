<?php

namespace Lmn\Calendar\Repository\Listener;

use Lmn\Core\Lib\Repository\EloquentRepository;
use Lmn\Core\Lib\Repository\AbstractListenerEloquentRepository;

class CalendareventruleListener extends AbstractListenerEloquentRepository {

    private $repo;

    public function __construct(EloquentRepository $repo) {
        $this->repo = $repo;
    }

    private function set($parentModel, $value) {
        $parentModel->calendareventrules = $value;
    }

    public function onGet($model) {
        if (!$model) {
            return;
        }

        $value = $this->repo->clear()
            ->criteria('calendarevent.ext.id', ['calendareventId' => $model->id])
            ->all();
        $this->set($model, $value);
    }

    public function onList($models) {
        $ids = [];
        foreach ($models as $m) {
            $ids[] = $m->id;
        }

        $list = $this->repo->clear()
            ->criteria('calendarevent.ext.ids', ['calendareventIds' => $ids])
            ->all();

        $listSorted = [];
        foreach ($list as $l) {
            if (!isset($listSorted[$l->calendarevent_id])) {
                $listSorted[$l->calendarevent_id] = [];
            }
            $listSorted[$l->calendarevent_id][] = $l;
        }
        foreach ($models as $model) {
            if (!isset($listSorted[$model->id])) {
                $this->set($model, []);
                continue;
            }
            $this->set($model, $listSorted[$model->id]);
        }
    }

    public function onCreate($model, $data) {
        if (!$model || !isset($data['calendareventrules'])) {
            return null;
        }

        $createData = $data['calendareventrules'];
        $createData['calendarevent_id'] = $model->id;

        $this->set($model, [$this->repo->create($createData)]);
    }

    public function onUpdate($model, $data) {
        if (!$model ||  !isset($data['calendareventrules'])) {
            return null;
        }

        $data['calendarevent_id'] = $model->id;
        $this->repo->clear()
            ->criteria('calendarevent.ext.id', ['calendareventId' => $model->id])
            ->delete();

        $this->onCreate($model, $data);
    }
}
