<?php

namespace Lmn\Calendar\Repository\Criteria\Calendarevent;

use Lmn\Core\Lib\Repository\Criteria\Criteria;
use Illuminate\Database\Eloquent\Builder;

class CalendareventExtensionIdsCriteria implements Criteria {

    private $calendareventIds;

    public function __construct() {

    }

    public function set($args) {
        $this->calendareventIds = $args['calendareventIds'];
    }

    public function apply(Builder $builder) {
        $builder->whereIn('calendarevent_id', $this->calendareventIds);
    }
}
