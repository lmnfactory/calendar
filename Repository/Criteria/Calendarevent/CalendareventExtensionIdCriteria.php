<?php

namespace Lmn\Calendar\Repository\Criteria\Calendarevent;

use Lmn\Core\Lib\Repository\Criteria\Criteria;
use Illuminate\Database\Eloquent\Builder;

class CalendareventExtensionIdCriteria implements Criteria {

    private $calendareventId;

    public function __construct() {

    }

    public function set($args) {
        $this->calendareventId = $args['calendareventId'];
    }

    public function apply(Builder $builder) {
        $builder->where('calendarevent_id', '=', $this->calendareventId);
    }
}
