<?php

namespace Lmn\Calendar\Repository\Criteria\Calendarevent;

use Lmn\Core\Lib\Repository\Criteria\Criteria;
use Illuminate\Database\Eloquent\Builder;

class CalendareventOrderDefaultCriteria implements Criteria {

    public function __construct() {

    }

    public function set($args) {

    }

    public function apply(Builder $builder) {
        $builder->orderBy(\DB::raw('TIME(eventstart)'), 'ASC');
    }
}
