<?php

namespace Lmn\Calendar\Repository\Criteria\Calendarevent;

use Lmn\Core\Lib\Repository\Criteria\Criteria;
use Illuminate\Database\Eloquent\Builder;

class CalendareventPublicIdCriteria implements Criteria {

    private $publicId;

    public function __construct() {

    }

    public function set($args) {
        $this->publicId = $args['publicId'];
    }

    public function apply(Builder $builder) {
        $builder->where('public_id', '=', $this->publicId);
    }
}
