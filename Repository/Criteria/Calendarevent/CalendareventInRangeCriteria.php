<?php

namespace Lmn\Calendar\Repository\Criteria\Calendarevent;

use Lmn\Core\Lib\Repository\Criteria\Criteria;
use Illuminate\Database\Eloquent\Builder;

class CalendareventInRangeCriteria implements Criteria {

    private $since;
    private $until;

    public function __construct() {

    }

    public function set($args) {
        $this->since = $args['since'];
        $this->until = $args['until'];
    }

    public function apply(Builder $builder) {
        $self = $this;
        $builder->where('eventstart', '<=', $this->until)
            ->where(function($query) use ($self) {
                return $query->whereNull('eventend')
                    ->orWhere('eventend', '>=', $self->since);
            });
    }
}
