<?php

namespace Lmn\Calendar;

use Lmn\Core\Provider\LmnServiceProvider;
use Lmn\Core\Lib\Module\ModuleServiceProvider;
use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Application;

use Lmn\Calendar\Lib\Calendar\Extension\CalendareventruleCrudExtension;
use Lmn\Calendar\Lib\Calendar\Extension\CalendareventnoteCrudExtension;

use Lmn\Calendar\Lib\Calendar\CalendareventService;
use Lmn\Calendar\Lib\Calendar\Rule\EveryDay;
use Lmn\Calendar\Lib\Calendar\Rule\EveryWeek;
use Lmn\Calendar\Lib\Calendar\Rule\EveryWeekday;
use Lmn\Calendar\Lib\Calendar\Rule\EveryWeekend;
use Lmn\Calendar\Lib\Calendar\Rule\SingleDay;

use Lmn\Core\Lib\Database\Seeder\SeederService;
use Lmn\Core\Lib\Cache\CacheService;

use Lmn\Calendar\Database\Seed\CalendarSeeder;

use Lmn\Core\Lib\Model\ValidationService;
use Lmn\Core\Lib\Repository\Criteria\CriteriaService;
use Lmn\Calendar\Database\Validation\CalendareventValidation;
use Lmn\Calendar\Database\Validation\CalendareventruleValidation;
use Lmn\Calendar\Database\Validation\CalendareventnoteValidation;
use Lmn\Calendar\Database\Validation\FormCalendareventCreateValidation;
use Lmn\Calendar\Database\Validation\FormCalendareventUpdateValidation;

use Lmn\Calendar\Repository\CalendareventRepository;
use Lmn\Calendar\Repository\CalendareventruleRepository;
use Lmn\Calendar\Repository\CalendareventnoteRepository;
use Lmn\Calendar\Repository\Listener\CalendareventnoteListener;
use Lmn\Calendar\Repository\Listener\CalendareventruleListener;
use Lmn\Calendar\Repository\Criteria\Calendarevent\CalendareventInRangeCriteria;
use Lmn\Calendar\Repository\Criteria\Calendarevent\CalendareventOrderDefaultCriteria;
use Lmn\Calendar\Repository\Criteria\Calendarevent\CalendareventExtensionIdCriteria;
use Lmn\Calendar\Repository\Criteria\Calendarevent\CalendareventExtensionIdsCriteria;
use Lmn\Calendar\Repository\Criteria\Calendarevent\CalendareventPublicIdCriteria;

use Lmn\Account\Middleware\SigninRequiredMiddleware;

class ModuleProvider implements ModuleServiceProvider{

    public function register(LmnServiceProvider $provider) {
        /** @var Application */
        $app = $provider->getApp();

        $app->singleton(CalendareventService::class, function($app) {
            return new CalendareventService();
        });

        $app->singleton(CalendareventRepository::class, CalendareventRepository::class);
        $app->singleton(CalendareventruleRepository::class, CalendareventruleRepository::class);
        $app->singleton(CalendareventnoteRepository::class, CalendareventnoteRepository::class);
        $app->singleton(CalendareventruleRepositoryExtension::class, CalendareventruleRepositoryExtension::class);
        $app->singleton(CalendareventnoteRepositoryExtension::class, CalendareventnoteRepositoryExtension::class);
    }

    public function boot(LmnServiceProvider $provider) {
        $app = $provider->getApp();

        /** @var SeederService */
        $seederService = \App::make(SeederService::class);
        $seederService->addSeeder(CalendarSeeder::class);

        /** @var ValidationService $validationService */
        $validationService = \App::make(ValidationService::class);
        $validationService->add('calendarevent', CalendareventValidation::class);
        $validationService->add('calendareventrule', CalendareventruleValidation::class);
        $validationService->add('calendareventnote', CalendareventnoteValidation::class);
        $validationService->add('form.calendar.create', FormCalendareventCreateValidation::class);
        $validationService->add('form.calendar.update', FormCalendareventUpdateValidation::class);

        /** @var CalendareventService $calendareventService */
        $calendareventService = \App::make(CalendareventService::class);
        $calendareventService->addRuleHandler(EveryDay::class);
        $calendareventService->addRuleHandler(EveryWeek::class);
        $calendareventService->addRuleHandler(EveryWeekday::class);
        $calendareventService->addRuleHandler(EveryWeekend::class);
        $calendareventService->addRuleHandler(SingleDay::class);

        /** @var CriteriaService */
        $criteriaService = $app->make(CriteriaService::class);
        $criteriaService->add('calendarevent.ext.id', CalendareventExtensionIdCriteria::class);
        $criteriaService->add('calendarevent.ext.ids', CalendareventExtensionIdsCriteria::class);
        $criteriaService->add('calendarevent.by.range', CalendareventInRangeCriteria::class);
        $criteriaService->add('calendarevent.by.pid', CalendareventPublicIdCriteria::class);
        $criteriaService->add('calendarevent.order.default', CalendareventOrderDefaultCriteria::class);

        /** @var CalendareventRepository $calendareventRepository */
        $calendareventRepository = $app->make(CalendareventRepository::class);
        $calendareventRepository->on('*', new CalendareventruleListener($app->make(CalendareventruleRepository::class)));
        $calendareventRepository->on('*', new CalendareventnoteListener($app->make(CalendareventnoteRepository::class)));
    }

    public function event(LmnServiceProvider $provider) {

    }

    public function route(LmnServiceProvider $provider) {
        Route::group(['namespace' => 'Lmn\\Calendar\\Controller'], function() {
            
        });
    }
}
