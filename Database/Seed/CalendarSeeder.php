<?php

namespace Lmn\Calendar\Database\Seed;

use App;
use Illuminate\Database\Seeder;

class CalendarSeeder extends Seeder {

    public function run() {
        $env = App::environment();
        if ($env == "production") {
            return;
        }
        \DB::table('calendarevent')->insert([
            [
                'id' => 1,
                'name' => 'prednaska',
                'description' => 'povinna prednaska',
                'location' => 'prednaskova miestnost C',
                'eventstart' => '2017-05-01 10:00:00',
                'eventend' => null,
                'duration' => 3600,
                'priority' => 3,
            ],
            [
                'id' => 2,
                'name' => 'cviko',
                'description' => 'meh cviko',
                'location' => 'ucebna A12',
                'eventstart' => '2017-03-01 16:00:00',
                'eventend' => null,
                'duration' => 6000,
                'priority' => 3,
            ],
            [
                'id' => 3,
                'name' => 'zapocet',
                'description' => 'zapocet za 20 bodov',
                'location' => 'prednaskova miestnost C',
                'eventstart' => '2017-06-13 14:30:00',
                'eventend' => null,
                'duration' => 7200,
                'priority' => 5,
            ]
        ]);

        \DB::table('calendareventrule')->insert([
            [
                'id' => 1,
                'calendarevent_id' => 1,
                'rule' => 'ew',
                'exception' => 0,
                'note' => null
            ],
            [
                'id' => 2,
                'calendarevent_id' => 2,
                'rule' => 'ew',
                'exception' => 0,
                'note' => null
            ],
            [
                'id' => 3,
                'calendarevent_id' => 3,
                'rule' => 'sd',
                'exception' => 0,
                'note' => null
            ],
        ]);
    }
}
