<?php

namespace Lmn\Calendar\Database\Validation;
use Lmn\Core\Lib\Model\LaravelValidation;

class FormCalendareventCreateValidation extends LaravelValidation {

    public function getRules($data) {
        return [
            'name' => 'required|max:255',
            'description' => '',
            'location' => 'max:255',
            'eventstart' => 'required|date_format:"Y-n-j H:i:s"',
            'eventend' => '',
            'duration' => 'required|numeric',
            'priority' => 'required|numeric',
            'calendareventrules' => 'array',
            'calendareventrules.rule' => 'required|max:255',
            'calendareventrules.exception' => 'boolean',
            'calendareventsettings' => 'array',
            'calendareventsettings.private' => 'boolean',
        ];
    }
}
