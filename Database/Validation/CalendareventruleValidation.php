<?php

namespace Lmn\Calendar\Database\Validation;
use Lmn\Core\Lib\Model\LaravelValidation;

class CalendareventruleValidation extends LaravelValidation {

    public function getRules($data) {
        return [
            'rule' => 'required|max:255'
        ];
    }
}
