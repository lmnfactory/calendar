<?php

namespace Lmn\Calendar\Database\Validation;
use Lmn\Core\Lib\Model\LaravelValidation;

class CalendareventValidation extends LaravelValidation {

    public function getRules($data) {
        return [
            'name' => 'required|max:255',
            'description' => '',
            'location' => 'max:255',
            'eventstart' => 'required|date',
            'eventend' => '',
            'duration' => '',
            'priority' => 'numeric'
        ];
    }
}
