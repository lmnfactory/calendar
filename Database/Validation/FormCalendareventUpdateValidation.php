<?php

namespace Lmn\Calendar\Database\Validation;

class FormCalendareventUpdateValidation extends FormCalendareventCreateValidation {

    public function __construct() {

    }

    public function getRules($data) {
        $rules = parent::getRules($data);
        $rules['public_id'] = 'required|exists:calendarevent,public_id';
        return $rules;
    }
}
