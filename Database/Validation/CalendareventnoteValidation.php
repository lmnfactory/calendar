<?php

namespace Lmn\Calendar\Database\Validation;
use Lmn\Core\Lib\Model\LaravelValidation;

class CalendareventnoteValidation extends LaravelValidation {

    public function getRules($data) {
        return [
            'calendareven_id' => 'required|exists:calendarevent,id',
            'date_at' => 'required|date',
            'note' => 'required'
        ];
    }
}
