<?php

namespace Lmn\Calendar\Database\Model;

use Illuminate\Database\Eloquent\Model;

class Calendareventnote extends Model {

    protected $table = 'calendareventnote';

    protected $fillable = ['calendarevent_id', 'date_at', 'note'];
}
