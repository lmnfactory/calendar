<?php

namespace Lmn\Calendar\Database\Model;

use Illuminate\Database\Eloquent\Model;
use Lmn\Calendar\Lib\Calendar\CalendarEventRule as RuleStructure;

class Calendareventrule extends Model {

    private $_rule = null;

    protected $table = 'calendareventrule';

    protected $fillable = ['calendarevent_id', 'rule', 'exception', 'note'];

    public function getRule() {
        if ($this->_rule == null) {
            $this->_rule = new RuleStructure($this->rule);
        }
        return $this->_rule;
    }
}
