<?php

namespace Lmn\Calendar\Database\Model;

use Illuminate\Database\Eloquent\Model;

class Calendarevent extends Model {

    protected $table = 'calendarevent';

    protected $fillable = ['name', 'description', 'location', 'eventstart', 'eventend', 'duration', 'priority', 'allday'];

    public function getEventend() {
        return (\strtotime($this->eventstart) + $this->duration);
    }
}
