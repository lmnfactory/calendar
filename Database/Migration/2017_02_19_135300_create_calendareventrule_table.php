<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCalendareventruleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calendareventrule', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('calendarevent_id')->unsigned();
            $table->char('rule', 255);
            $table->boolean('exception')->default(0);
            $table->char('note', 255)->nullable();
            $table->timestamps();

            $table->index('calendarevent_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('calendareventrule');
    }
}
