<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCalendareventnoteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calendareventnote', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('calendarevent_id')->unsigned();
            $table->dateTime('date_at');
            $table->text('note');
            $table->timestamps();

            $table->index('calendarevent_id');
            $table->index('date_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('calendareventnote');
    }
}
