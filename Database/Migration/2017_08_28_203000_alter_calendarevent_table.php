<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use Lmn\Core\Lib\Facade\Generator;

class AlterCalendareventTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('calendarevent', function (Blueprint $table) {
            $table->char('public_id', 24);

            $table->index('public_id');
        });

        $rows = \DB::table('calendarevent')->get();
        foreach ($rows as $row) {
            $publicId = Generator::uniqueString('calendarevent', 'public_id', 24);
            \DB::table('calendarevent')
                ->where('id', $row->id)
                ->update([
                    'public_id' => $publicId
                ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('calendarevent', function (Blueprint $table) {
            $table->dropColumn('public_id');

            $table->dropIndex('public_id');
        });
    }
}
