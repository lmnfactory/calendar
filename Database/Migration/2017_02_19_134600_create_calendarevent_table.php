<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCalendareventTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calendarevent', function (Blueprint $table) {
            $table->increments('id');
            $table->char('name', 255);
            $table->text('description')->nullable();
            $table->char('location', 255)->nullable();
            $table->dateTime('eventstart');
            $table->dateTime('eventend')->nullable();
            $table->integer('duration');
            $table->integer('priority')->default(3);
            $table->boolean('allday')->default(0);
            $table->timestamps();

            $table->index('name');
            $table->index('eventstart');
            $table->index('eventend');
            $table->index('created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('calendarevent');
    }
}
