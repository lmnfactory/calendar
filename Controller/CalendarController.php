<?php

namespace Lmn\Calendar\Controller;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Lmn\Core\Lib\Response\ResponseService;
use Lmn\Core\Lib\Model\ValidationService;
use Lmn\Calendar\Repository\CalendareventRepository;

class CalendarController extends Controller {

    public function addRule(Request $request, ResponseService $responseService, ValidationService $validationService) {
        $data = $request->json()->all();

        if (!$validationService->validate($data, 'form.calendar.addRule')) {
            return $responseService->use('validation.data');
        }

        return $responseService->response($calendarevent->toArray());
    }
}
