<?php

namespace Lmn\Calendar\Lib\Calendar;

use Lmn\Calendar\Lib\Calendar\CalendarEventRule;

interface CalendareventruleHandler {
    public function getOccurences($calendarevent, CalendarEventRule $rule, \DateTime $fromDate, \DateTime $toDate);
    public function canHandle(CalendarEventRule $rule);
    public function getInterface();
}
