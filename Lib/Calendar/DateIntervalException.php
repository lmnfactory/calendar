<?php

namespace App\Module\lmn\calendar\lib\calendar;

class DateIntervalException extends \Exception {

    public function __construct($message, $previous = null) {
        parent::__construct($message, 500, $previous);
    }
}
