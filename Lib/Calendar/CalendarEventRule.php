<?php

namespace Lmn\Calendar\Lib\Calendar;

class CalendarEventRule {

    private $rule;
    private $name;
    private $params;

    public function __construct($rule = null) {
        $this->name = null;
        $this->params = [];
        $this->setRule($rule);
    }

    private function disassamble() {
        $exp = explode(":", $this->rule);
        $this->setName(array_shift($exp));
        if (sizeof($exp) > 0) {
            $params = implode(":", $exp);
            $this->addParams(explode(",", $params));
        }
    }

    private function assamble() {

    }

    public function setRule($rule) {
        $this->rule = $rule;
        if ($this->rule != null) {
            $this->disassamble();
        }
    }

    public function setName($name) {
        $this->name = $name;
        return $this;
    }

    public function addParam($param) {
        $this->params[] = $param;
        return $this;
    }

    public function addParams($params) {
        $this->params = array_merge($this->params, $params);
        return $this;
    }

    public function getName() {
        return $this->name;
    }

    public function getParams() {
        return $this->params;
    }

    public function make() {
        $this->assamble();
        return $this->rule;
    }
}
