<?php

namespace Lmn\Calendar\Lib\Calendar;

use Lmn\Core\Lib\Structure\StructureService;
use Lmn\Calendar\Database\Model\Calendarevent;

class CalendareventService {

    private $ruleHandlers;

    public function __construct() {
        /** @var StructureService $mapService */
        $arrayService = \App::make('arrayService');
        $this->ruleHandlers = $arrayService->make('unorderedSingleton');
    }

    private function getEventdate($event, $year, $month, $day) {
        $exp = explode(" ", $event->eventstart);
        $time = $exp[1];
        return $year."-".$month."-".$day." ".$time;
    }

    private function getOccurencesForEvent($calendarevent, \DateTime $since, \DateTime $until) {
        $handlers = $this->ruleHandlers->getAll();

        $days = [];

        foreach ($calendarevent->calendareventrules as $eventrule) {
            $occurences = null;
            foreach ($handlers as $h) {
                if ($h->canHandle($eventrule->getRule())) {
                    $occurences = $h->getOccurences($calendarevent, $eventrule->getRule(), $since, $until);
                    break;
                }
            }
            if ($occurences == null) {
                continue;
            }

            foreach ($occurences as $o) {
                if (isset($o['eventdate'])) {
                    $eventDate = $o['eventdate'];
                }
                else {
                    $eventDate = $o['date'];
                }
                $exp = explode("_", $eventDate);
                $days[$o['date']] = [
                    'event_id' => $calendarevent->id,
                    'rule_id' => $eventrule->id,
                    'eventdate' => $this->getEventdate($calendarevent, $exp[0], $exp[1], $exp[2])
                ];
            }
        }

        return $days;
    }

    public function addRuleHandler($ruleHandler) {
        $this->ruleHandlers->add($ruleHandler);
    }

    public function getEventsInRange($events, $since, $until) {
        $eventsInDays = [];
        foreach ($events as $e) {
            $days = $this->getOccurencesForEvent($e, $since, $until);

            foreach ($days as $date => $d) {
                if (!isset($eventsInDays[$date])) {
                    $eventsInDays[$date] = [];
                }

                $eventsInDays[$date][] = $d;
            }
        }

        return $eventsInDays;
    }
}
