<?php

namespace Lmn\Calendar\Lib\Calendar\Extension;

use Lmn\Core\Lib\Crud\AbstractCrudExtension;
use Lmn\Core\Lib\Database\Save\SaveBuilder;
use Lmn\Core\Lib\Model\ValidationService;
use Lmn\Core\Lib\Database\Select\ConditionCollection;
use Lmn\Calendar\Database\Model\Calendareventrule;

class CalendareventruleCrudExtension extends AbstractCrudExtension {

    public function create(SaveBuilder $builder, $model, $data) {
        /** @var ValidationService $validationService */
        $validationService = \App::make(ValidationService::class);

        if (isset($data['calendareventrules'])) {
            $ruleData = $data['calendareventrules'];
            $calendareventrule = new Calendareventrule();
            $validationService->systemValidate($ruleData, 'calendareventrule');
            $calendareventrule->fill($ruleData);
            $builder->has($calendareventrule, $model);
        }
    }

    public function delete($models) {
        $ids = [];
        foreach ($models as $m) {
            $ids[] = $m->id;
        }

        Calendareventrule::whereIn(['calendarevent_id' => $ids])->delete();
    }

    public function readDetail($model, ConditionCollection $condition) {
        $rules = Calendareventrule::where(['calendarevent_id' => $model->id])->get();

        $model->calendareventrules = $rules;
    }

    public function readList($models, ConditionCollection $condition) {
        $ids = [];
        foreach ($models as $m) {
            $ids[] = $m->id;
        }

        $rules = Calendareventrule::whereIn('calendarevent_id', $ids)->get();

        $sortRules = [];
        foreach ($rules as $r) {
            if (!isset($sortRules[$r->calendarevent_id])) {
                $sortRules[$r->calendarevent_id] = [];
            }
            $sortRules[$r->calendarevent_id][] = $r;
        }

        foreach ($models as $m) {
            if (!isset($sortRules[$m->id])) {
                $sortRules[$m->id] = [];
            }
            $m->calendareventrules = $sortRules[$m->id];
        }
    }
}
