<?php

namespace Lmn\Calendar\Lib\Calendar\Extension;

use Lmn\Core\Lib\Crud\AbstractCrudExtension;
use Lmn\Core\Lib\Database\Save\SaveBuilder;
use Lmn\Core\Lib\Model\ValidationService;
use Lmn\Calendar\Database\Model\Calendareventnote;
use Lmn\Core\Lib\Database\Select\ConditionCollection;

class CalendareventnoteCrudExtension extends AbstractCrudExtension {

    public function create(SaveBuilder $builder, $model, $data) {
        /** @var ValidationService $validationService */
        $validationService = \App::make(ValidationService::class);

        if (isset($data['calendareventnotes'])) {
            foreach ($data['calendareventnotes'] as $noteData) {
                $calendareventnote = new Calendareventnote();
                $validationService->systemValidate($noteData, 'calendareventnote');
                $calendareventnote->fill($noteData);
                $builder->has($calendareventnote, $model);
            }
        }
    }

    public function delete($models) {
        if (!is_array($models)) {
            $models = [$models];
        }
        $ids = [];
        foreach ($models as $m) {
            $ids[] = $m->id;
        }

        Calendareventnote::whereIn(['calendarevent_id' => $ids])->delete();
    }

    public function readDetail($model, ConditionCollection $condition) {
        $rules = Calendareventnote::where(['calendarevent_id' => $model->id])->get();

        $model->calendareventnote = $rules;
    }

    public function readList($models, ConditionCollection $condition) {
        $ids = [];
        foreach ($models as $m) {
            $ids[] = $m->id;
        }
    }
}
