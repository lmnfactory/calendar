<?php

namespace Lmn\Calendar\Lib\Calendar\Rule;

use Lmn\Calendar\Lib\Calendar\AbstractCalendareventruleHandler;
use Lmn\Calendar\Lib\Calendar\CalendarEventRule;

class EveryWeekday extends AbstractCalendareventruleHandler {

    public function __construct() {

    }

    //TODO: fix eventstart (eventend) condition
    public function handle($calendarevent, CalendarEventRule $rule, \DateTime $fromDate, \DateTime $toDate) {
        $dayInterval = new \DateInterval("P1D");
        $dates = [];
        while ($fromDate <= $toDate) {
            $day = ($fromDate->format("w") - 1) % 7;
            if (!$day == 6 && $day != 0) {
                $dates[] = $fromDate->format("Y_m_d");
            }

            $fromDate->add($dayInterval);
        }

        return $dates;
    }

    public function canHandle(CalendarEventRule $rule) {
        return ($rule->getName() == "ewday");
    }

    public function getInterface() {

    }
}
