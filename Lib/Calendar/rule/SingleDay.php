<?php

namespace Lmn\Calendar\Lib\Calendar\Rule;

use Lmn\Calendar\Lib\Calendar\AbstractCalendareventruleHandler;
use Lmn\Calendar\Lib\Calendar\CalendarEventRule;

class SingleDay extends AbstractCalendareventruleHandler {

    public function handle($calendarevent, CalendarEventRule $rule, \DateTime $fromDate, \DateTime $toDate) {
        $date = new \DateTime($calendarevent->eventstart);
        
        return [ $date->format("Y_m_d") ];
    }

    public function canHandle(CalendarEventRule $rule) {
        return ($rule->getName() == "sd");
    }

    public function getInterface() {

    }
}
