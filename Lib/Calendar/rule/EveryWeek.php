<?php

namespace Lmn\Calendar\Lib\Calendar\Rule;

use Lmn\Calendar\Lib\Calendar\AbstractCalendareventruleHandler;
use Lmn\Calendar\Lib\Calendar\CalendarEventRule;

class EveryWeek extends AbstractCalendareventruleHandler {

    public function __construct() {

    }

    //TODO: fix eventend condition
    public function handle($calendarevent, CalendarEventRule $rule, \DateTime $fromDate, \DateTime $toDate) {
        $params = $rule->getParams();
        if (sizeof($params) == 0) {
            $dayParam = \DateTime::createFromFormat('Y-n-j H:i:s', $calendarevent->eventstart)->format('N') - 1;
        }
        else {
            $dayParam = $params[0];
        }

        $dayInterval = new \DateInterval("P1D");
        $weekInterval = new \DateInterval("P1W");
        $dates = [];

        // Find day in week
        while ($fromDate <= $toDate) {
            $day = ($fromDate->format("N") - 1);
            if ($day == $dayParam) {
                break;
            }
            $fromDate->add($dayInterval);
        }

        // Fill dates
        while ($fromDate <= $toDate) {
            if ($fromDate->format("Y-m-d 23:59:59") >= $calendarevent->eventstart) {
                $dates[] = $fromDate->format("Y_m_d");
            }

            $fromDate->add($weekInterval);
        }

        return $dates;
    }

    public function canHandle(CalendarEventRule $rule) {
        return ($rule->getName() == "ew");
    }

    public function getInterface() {

    }
}
