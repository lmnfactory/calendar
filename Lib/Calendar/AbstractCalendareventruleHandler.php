<?php

namespace Lmn\Calendar\Lib\Calendar;

abstract class AbstractCalendareventruleHandler implements CalendareventruleHandler {

    private $fromTimestamp;
    private $toTimestamp;

    public function __construct() {
        $this->fromTimestamp = 0;
        $this->toTimestamp = 0;
    }

    public function prepareDates(\DateTime $fromDate, \DateTime $toDate) {
        $this->fromTimestamp = $fromDate->getTimestamp();
        $this->toTimestamp = $toDate->getTimestamp();

        $fromDate->setTime(0, 0 , 0);
        $toDate->setTime(23, 59, 59);

        if ($toDate < $fromDate) {
            throw new DateIntervalException("Start date is bigger then end date");
        }
    }

    public function resetDates($fromDate, $toDate) {
        $fromDate->setTimestamp($this->fromTimestamp);
        $toDate->setTimestamp($this->toTimestamp);
    }

    public function getOccurences($calendarevent, CalendarEventRule $rule, \DateTime $fromDate, \DateTime $toDate) {
        try {
            $this->prepareDates($fromDate, $toDate);
        }
        catch (DateIntervalException $ex) {
            return [];
        }

        $dates = $this->handle($calendarevent, $rule, $fromDate, $toDate);
        
        $durationInterval = new \DateInterval('PT' . $calendarevent->duration . "S");
        $eventStartObject = \DateTime::createFromFormat("Y-n-j H:i:s", $calendarevent->eventstart);
        $eventEndObject = \DateTime::createFromFormat("Y-n-j H:i:s", $calendarevent->eventstart);
        $eventEndObject->add($durationInterval);
        $eventEndDate = $eventEndObject->format("Y-n-j");
        if ($eventStartObject->format("Y-n-j") != $eventEndDate) {
            $dayInterval = new \DateInterval("P1D");
            $count = 0;
            while (true) {
                $count++;
                if ($eventStartObject->format("Y-n-j") == $eventEndDate) {
                    break;
                }
                $eventStartObject->add($dayInterval);
            }
            $newDates = [];
            foreach ($dates as $date) {
                $dateObject = \DateTime::createFromFormat("Y_m_d", $date);
                for ($i = 0; $i < $count; $i++) {
                    $newDates[] = [
                        'date' => $dateObject->format("Y_m_d"),
                        'eventdate' => $date
                    ];
                    $dateObject->add($dayInterval);
                }
            }
            $dates = $newDates;
        }
        else {
            foreach ($dates as $date) {
                $newDates[] = [
                    'date' => $date
                ];
            }
            $dates = $newDates;
        }

        $this->resetDates($fromDate, $toDate);
        return $dates;
    }

    abstract public function handle($calendarevent, CalendarEventRule $rule, \DateTime $fromDate, \DateTime $toDate);
}
