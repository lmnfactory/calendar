<?php

namespace Lmn\Calendar\Build\Test;

use Lmn\Account\Lib\Test\AuthTestCase;

use Illuminate\Foundation\Testing\DatabaseTransactions;

class CalendarControllerTest extends AuthTestCase {

    use DatabaseTransactions;

    public function testCreate() {
        $this->authJson('POST', '/api/calendar/create', [
                'name' => 'test name',
                'eventstart' => '2017-06-30 10:00:00',
                'duration' => 3600,
                'priority' => 3,
                'calendareventrules' => [
                    'rule' => 'sd'
                ]
            ])
            ->assertResponseOk()
            ->seeJsonStructure([
                'data' => [
                    'id',
                    'public_id',
                    'name',
                    'calendareventrules'
                ]
            ]);
    }

    public function testCreateAllValues() {
        $this->authJson('POST', '/api/calendar/create', [
                'name' => 'test name',
                'description' => 'description value',
                'location' => 'room A101',
                'eventstart' => '2017-06-30 10:00:00',
                'eventend' => null,
                'duration' => 3600,
                'priority' => 3,
                'calendareventsettings' => [
                    'private' => false,
                ],
                'calendareventrules' => [
                    'rule' => 'sd',
                    'exception' => false
                ]
            ])
            ->assertResponseOk()
            ->seeJsonStructure([
                'data' => [
                    'id',
                    'public_id',
                    'name',
                    'calendareventrules',
                    'calendareventsettings'
                ]
            ]);
    }

    public function testCreateMissingName() {
        $this->authJson('POST', '/api/calendar/create', [
                'eventstart' => '2017-06-30 10:00:00',
                'duration' => 3600,
                'priority' => 3,
                'calendareventrules' => [
                    'rule' => 'sd'
                ]
            ])
            ->assertResponseStatus(422);
    }

    public function testCreateMissingEventstart() {
        $this->authJson('POST', '/api/calendar/create', [
                'name' => 'test name',
                'duration' => 3600,
                'priority' => 3,
                'calendareventrules' => [
                    'rule' => 'sd'
                ]
            ])
            ->assertResponseStatus(422);
    }

    public function testCreateMissingDuration() {
        $this->authJson('POST', '/api/calendar/create', [
                'name' => 'test name',
                'eventstart' => '2017-06-30 10:00:00',
                'priority' => 3,
                'calendareventrules' => [
                    'rule' => 'sd'
                ]
            ])
            ->assertResponseStatus(422);
    }

    public function testCreateMissingPriority() {
        $this->authJson('POST', '/api/calendar/create', [
                'name' => 'test name',
                'eventstart' => '2017-06-30 10:00:00',
                'duration' => 3600,
                'calendareventrules' => [
                    'rule' => 'sd'
                ]
            ])
            ->assertResponseStatus(422);
    }

    public function testCreateMissingRule() {
        $this->authJson('POST', '/api/calendar/create', [
                'name' => 'test name',
                'eventstart' => '2017-06-30 10:00:00',
                'duration' => 3600,
                'priority' => 3,
                'calendareventrules' => []
            ])
            ->assertResponseStatus(422);
    }

    public function testCreateInvalidEventstart() {
        $this->authJson('POST', '/api/calendar/create', [
                'name' => 'test name',
                'eventstart' => '2017-0630 10:00:00',
                'duration' => 3600,
                'priority' => 3,
                'calendareventrules' => [
                    'rule' => 'sd'
                ]
            ])
            ->assertResponseStatus(422);
    }

    public function testCreateUnauthorise() {
        $this->json('POST', '/api/calendar/create', [
                'name' => 'test name',
                'eventstart' => '2017-06-30 10:00:00',
                'duration' => 3600,
                'priority' => 3,
                'calendareventrules' => [
                    'rule' => 'sd'
                ]
            ])
            ->assertResponseStatus(401);
    }
}
